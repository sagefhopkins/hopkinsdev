import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import Nav from './components/nav.js';
import CssBaseline from '@mui/material/CssBaseline';
import Theme from './assets/theme.js';
import {ThemeProvider} from '@mui/material/styles';
import Heading from './components/heading.js';
import {Break, Cards, Break2, About, Break3, Portfolio, Footer} from './components/body.js';


const theme = Theme();

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
    <CssBaseline />
    <head>
        <meta name='viewport' content='initial-scale=1, width=device-width' />
        <title>Hopkins Development</title>
        <link 
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
    </head>
    <Nav />
    <Heading />
    <Break />
    <Cards id="services"/>
    <Break2 />
    <About id="about"/>
    <Break3 />
    <Portfolio id="portfolio"/>
    <Footer id="contact"/>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
