import * as React from 'react';
import { createTheme,ThemeProvider } from '@mui/material/styles';

const theme = createTheme ({
    palette: {
        background: {
            default: "#000000"
        },
        primary: {
            light: '#ffff',
            main: '#000000',
            dark: '#002884',
            contrastText: '#fff',
        }
    }
});
export default function Theme(){
    return theme;
}
