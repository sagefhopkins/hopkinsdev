import * as React from 'react';
import reactDOM from 'react-dom';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

export default function App(){
    return(
        <Container maxWidth="xl">
            <Box display="flex" justifyContent="center" alignItems="center" minHeight="50vh" flexDirection="column">
                <Typography variant="h2" color="white" fontWeight="500" sx={{paddingBottom: '10vh'}}>
                    Amplify your first impression.
                </Typography>
                
            </Box>
        </Container>


    );

}
