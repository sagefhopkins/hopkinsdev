import * as React from 'react';
import reactDOM from 'react-dom';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Image from 'mui-image';
import surfShop from '../assets/images/surfshop.png';
import consulting from '../assets/images/consulting.png';
import computrio from '../assets/images/computrio.png';
import mauro from '../assets/images/mauro.png';
import residence from '../assets/images/residence.png';
import burrito from '../assets/images/burrito.png';
import PortfolioDialog from './bodyDialog.js';


const cards = [
        [
            "Design",
            "Level up your business with a professional website from Hopkins Development. The average user interacts with a web page for 15 seconds before leaving. We specialize in creating bold, meaningful and elegant websites to capture the attention of users, increasing web traffic and revenue."
        ], 
        [
            "Hosting",
            "Increase tempo, web hosting with Hopkins Development couldn't be quicker; Spend less of your 15 seconds loading and more time interacting. We use bleeding edge technologies to write fast, responsive and efficient webpages reducing loading times and increasing interaction with your business."
        ], 
        [
            "Support",
            "Uptime is important, Hopkins Development is ready to support your web emergencies around the clock. Your online business is important, downtime causes lost revenue which hurts your business. Whether you're an existing customer, or having a emergency Hopkins Development has your back."
        ] 
];
const portfolio = [
        [
            "Surf Shop",
            "A simple but bold website design, for a surf board shop. This site features bold, modern colors and fonts.",
            surfShop,
            true,
            'http://104.238.157.49/surf-shop',
            "A website design I created as part of a challenge I had for myself to create a new and unique website design each week in order to sharpen my design skills, and try new colors and techniques. This site featured very bold colors and fonts to draw a viewers vision into what I wanted them to see. This practice is part of Hopkins Developments effort to design bold, modern and creative websites which capture a viewers attention.",
            'HTML5, CSS, JavaScript'
        ],
        [
            "Consulting Firm",
            "This advanced business website features a colorful yet neutral palette used to draw the viewers attention.",
            consulting,
            true,
            'http://104.238.157.49/consulting-firm/',
            'This website design was created as part of a challenge I had for myself to create a new and unique website design each week. In this week I focused on using bold colors and fonts in order to draw the viewers attention to important sales headings. I also practices the use of linear gradients in webpages for beutiful design aspects which give texture to a website. This practice is how we at Hopkins Development capture a users attention.',
            'HTML5, CSS, JavaScript'
        ],
        [
            "Computr.io",
            "A fullstack web application which aimed to modernize the Computer Repair industry.",
            computrio,
            true,
            'http://104.238.157.49:3001/',
            "Computr.io is a fullstack web application for simplifying Computer Repair. It features a dashboard for tracking both client and technician data in order to keep everyone up to date. It also has secure user authentication and databasing. Although the project wasn't taken past the basic outlines of the website, it still represents an important application as it could be easily converted into a variety of client applications.",
            'Python, Flask, MySQL, HTML5, BootSrap, JavaScript, Linux'
        ],
        [
            "Mauro Enterprises",
            "Mauro Enterprices badly needed an update to their corporate website. The results, a modern business website.",
            mauro,
            true,
            'https://mauroenterprises.com/',
            'Mauro Enterprices hired me to update their existing website ahead of a new round of funding. This website features updated functionaility associated with a modern HTML5 website, and updated design aspects to keep the somewhat dated website up to date with their competitors.',
            'HTML5, CSS, JavaScript, Linux'
        ],
        [
            "Residence Renewed",
            "A fullstack web application for Home Repair/Updates. It's simple interface makes it a breeze to use.",
            residence,
            false,
            residence,
            'Residence Renewed is a website I was hired to build, although the client failed to fund the project, the framework of the site shows how we take a simple color palette, icons and fonts and create a modern website which is easy for a user to interact with.',
            'Python, Flask, MySQL, HTML5, JavaScript, Linux'
        ],
        [
            "Burrito Locator",
            "This web application plots the locations where Mauro Burritos are sold, allowing customers to find them nereby.",
            burrito,
            false,
            burrito,
            'Burrito Locator is a web application I was hired to create using Google Maps to plot the locations where Burritos from Mauro Burritos are sold. The application features both a client side application, as well as a simple administrator portal for adding additional locations.',
            'Python, Flask, MySQL, HTML5, JavaScript, Google Maps API, Linux'
        ]
];
export function Break(){
    return(
        <Box
        sx={{
            width: '100%',
            height: '25vh',
            background: 'rgb(0,0,0)', 
            background: 'linear-gradient(2deg, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 49%, rgba(0,0,0,1) 49.5%, rgba(0,0,00,1)  100%)'
        }}>
        </Box>
    );
}
export function Cards(){
    return (
        <Box
        id="services"
        sx={{
            width: '100%',
            minHeight: '45vh',
            background: 'rgb(255,255,255)'
        }}>
        <Typography variant="h2"  align="center" sx={{color: "rgb(40,196,216)"}}>
            TURN UP THE VOLUME
        </Typography>
        <Typography variant="h4" align="center" sx={{paddingBottom: '15px'}}>
            WE'RE YOUR LOCAL WEB EXPERTS
        </Typography>
            <Grid container spacing={2} columns={{ xs: 4, sm: 4, md:12 }} sx={{
                width: '100%',
                paddingLeft: '15vw',
                paddingRight: '15vw'
            }}>
        {/*Different resolutions can cause cards to no longer display uniformly. Appears to be some kind of height issue*/}
                {cards.map((heading) => (
                    <Grid item xs={4} sm={4} md={4} key={heading}>
                        <Card sx={{
                            background: 'rgb(0,0,0)',
                            color: 'rgb(255,255,255)',
                            minHeight: '22vh',
                            boxShadow: '5px 10px 0 3px rgb(40,196,216)',
                            borderRadius: '0'
                        }}>
                            <Typography variant="h4" align="center">
                                {heading[0]}
                            </Typography>
                            <Divider variant="middle" sx={{backgroundColor: 'rgb(255,255,255)'}}/>
                            <Typography paragraph='true' align="center" sx={{paddingLeft: '15%', paddingRight: '15%', paddingTop: '25px'}}>
                                {heading[1]}
                            </Typography>
                        </Card>
                    </Grid>
                ))}
            </Grid>
        </Box>
    );
}
export function Break2(){
    return (
        <Box 
        sx={{
            width: '100%',
            minHeight: '25vh',
            background: 'linear-gradient(-2deg, rgb(40, 196, 216) 0%, rgb(40, 196, 216) 49%, rgb(255, 255, 255) 49.5%, rgb(255, 255, 255) 100%)'
        }}
        />
    );
}
export function Break3(){
    return (
        <Box
        sx={{
            width: '100%',
            minHeight: '10vh',
            /*
            background: 'linear-gradient(0deg, rgb(255, 255, 255) 0%, rgb(40,196,216) 100%)'
            */
            background: 'rgb(40,196,216)'
            }}/>
    );
}

export function About(){
    return (
        <Box 
        sx={{
            width: '100%',
            minHeight: '25vh',
            background: 'rgb(40, 196, 216)',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
        {/*
        Matthew fix alignment issues when on mobile devices. Grid doesn't always work properly, some resolutions create large line of characters which doesn't look right in second grid item.
        Material UI probably has a better way of centering and aligning grids manually, that would likely fix this issue. A grid might not even be the solution required here, the probably could
        be various things but overall this block should be formatted in the following way:

                                                                      Heading
                                                                BOLD
                                                                CREATIVE    Text
                                                                DESIGN
        */}
            <Typography 
                variant="h2" 
                align="center"
                id="about" 
                sx={{
                    display: 'inline',
                    background: 'rgb(0,0,0)',
                    color: 'rgb(255,255,255)',
                    paddingLeft: '5px',
                    paddingRight: '5px'
                }}>
                WHO WE ARE
            </Typography>
            <Typography variant="h5" align="center" sx={{color: 'rgb(255,255,255)', padding: '10px', minWidth: '55%', maxWidth: '100%'}}>
                "We are a Veteran owned small business founded on the belief that bold creativity upheld by strategic design has a big impact in a modern world full of screens.
                Hopkins Development builds custom web applications align with your business, reduce workload and help your business scale to its full potential. We design custom 
                enterprise grade websites, designed around you business and it's goals. We've never been template builders, but rather design unique, creative and bold websites,
                of which no two are the same."
            </Typography>
            <Typography variant="h6" align="center" sx={{color: 'rgb(255,255,255)', paddingTop: '5px', fontStyle: 'italic'}}>
                -Sage Hopkins (Owner: Hopkins Development)
            </Typography>
        </Box>
    );

}
export function Portfolio(){
    return (
        <Box
        sx={{
            width: '100%',
            minHeight: '100vh',
            background: 'rgb(255,255,255)',
            paddingBottom: '15px'
        }}>
        <Typography 
                variant="h2" 
                align="center"
                sx={{
                    color: 'rgb(0,0,0)',
                    paddingTop: '25px'
                }}
                id="portfolio"
            >
                OUR WORK
            </Typography>
            <Typography
                variant="h4"
                align="center"
                sx={{
                    color: 'rgb(40,196,216)',
                    paddingBottom: '15px'
                }}
            >
                EXPERT DESIGNS, EXECUTED PERFECTLY
            </Typography>
            <Grid container spacing={2} columns={{ xs: 4, sm: 4, md:12 }} sx={{
                width: '100%',
                paddingLeft: '15vw',
                paddingRight: '15vw'
            }}>
        {/*Different resolutions can cause cards to no longer display uniformly. Appears to be some kind of height issue*/}
                {portfolio.map((heading) => (
                    <Grid item xs={4} sm={4} md={4} key={heading}>
                        <Card sx={{
                            background: 'rgba(0,0,0, .1)',
                            color: 'rgb(0,0,0)',
                            minHeight: '22vh',
                        }}>
                            <Typography variant="h4" align="center">
                                {heading[0]}
                            </Typography>
                            <Image 
                                src={heading[2]} height='27vh'></Image>
                            <Typography paragraph='true' align="center" sx={{paddingLeft: '15%', paddingRight: '15%', paddingTop: '25px'}}>
                                {heading[1]}
                            </Typography>
                            <PortfolioDialog 
                                title={heading[0]}
                                iframe={heading[5]}
                                url={heading[4]}
                                description={heading[5]}
                                software={heading[6]}
                            >
                            </PortfolioDialog>
                        </Card>
                    </Grid>
                ))}
            </Grid>
        </Box>
    );
}
export function Footer(){
    return(
        <Box sx={{background: 'rgb(0,0,0)', width: '100%', height: '15vh'}}>
            <Typography variant='h4' align="center" id="contact" sx={{color: 'rgb(255,255,255)', paddingTop: '5px'}}>CONTACT US</Typography>
            <Typography variant="h6" align="center" sx={{color: 'rgb(40,196,216)'}}>Email: sage@hopkinsdev.net</Typography>
            <Typography variant="h6" align="center" sx={{color: 'rgb(40,196,216)'}}>Phone #: (541) 841-2216</Typography>
        </Box>
    );
}

