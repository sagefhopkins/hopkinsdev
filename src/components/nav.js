import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Menu from '@mui/material/Menu';
import Container from '@mui/material/Container';
import MenuItem from '@mui/material/MenuItem';
import {ThemeProvider, createTheme } from '@mui/material/styles';
import Theme from '../assets/theme.js';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import Slide from '@mui/material/Slide';
import Logo from '../assets/logoSkyBlue.png';
import Image from 'mui-image';
import Dialog from '@mui/material/Dialog';
import { CardMedia, DialogActions, DialogContent, DialogCotent, DialogTitle } from '@mui/material';



function HideOnScroll(props){
    const {children, window} = props;
    const trigger = useScrollTrigger({
        target: window ? window() : undefined,
    });

    return (
        <Slide appear={false} direction="down" in={!trigger}>
            {children}
        </Slide>
    );
}

const theme = Theme();
const pages = 
    [
    ['Our Services', '#services'],
    ['About Us', '#about'],
    ['Our Work', '#portfolio'],
    ['Contact Us', '#contact']
    ];

const ResponsiveAppBar = () => {
    const [open, setOpen] = useState(false);
    const handleClickOpen = () =>{
    setOpen(true);
}
const handleClickClose = () =>{
    setOpen(false);
}
    const [anchorElNav, setAnchorElNav] = React.useState(null);
    
    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    return (
        <ThemeProvider theme={theme}>
        <AppBar position="static" HideOnScroll>
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Typography variant="h6" noWrap component="div" sx={{mr:2,display:{xs:'none',md:'flex'}}}>
                    <Image src={Logo} height="10vh" />
                    </Typography>
                    <Box sx={{flexGrow:1, display:{xd:'flex', md:'none'}}}>
                        <IconButton 
                            size="large" 
                            aria-label="account of current user" 
                            aria-controls="menu-appbar" 
                            aria-haspopup="true" 
                            onClick={handleOpenNavMenu}
                        >
                            <MenuIcon sx={{color: "white"}} />
                        </IconButton>
                        <Menu 
                            id="menu-appbar"
                            anchorEl={anchorElNav} 
                            anchorOrigin={{vertical:'bottom', horizontal: 'left',}}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu} 
                            sx={{display:{xd:'block', md:'none'},}}
                        >
                            {pages.map((page) =>(
                                <MenuItem key={page} onClick={handleCloseNavMenu}>
                                    <Typography textAlign="center">{page[0]}</Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                    <Typography
                        variant="h6"
                        noWrap
                        component="div"
                        sx={{flexGrow: 1, display: {xs: 'flex', md: 'none'}}}
                    >
                        HOPKINS DEVELOPMENT
                    </Typography>
                    <Box justifyContent="flex-end" sx={{flexGrow: 1, display: {xs: 'none', md: 'flex'}}}>
                        {pages.map((page) => (
                            <Button
                            key={page}
                            onClick={() =>{
                                const section = document.querySelector(page[1]);
                                section.scrollIntoView({behavior: 'smooth', block: 'start'});
                            }}
                            sx={{my:2, color: 'white', display: 'block'}}
                        >
                            {page[0]}
                        </Button>
                        ))}
                        <Button onClick={handleClickOpen} sx={{my: 2, color: 'white', display: 'block', border: '1px solid white'}}>Request Quote</Button>
                    </Box>
                </Toolbar>
            </Container>
            <Dialog
                open={open}
                onClose={handleClickClose}
                fullWidth="m"
                maxWidth="m">
                    <DialogTitle variant="h2" align="center">Test</DialogTitle>
                    <DialogContent>
                        <div align="center">
                            <CardMedia
                                component="iframe"
                                src="https://docs.google.com/forms/d/e/1FAIpQLSfe1GWmvGN2w_WyoWNcLDaEabdaFqMwZ0GdVxBgIrGr9bPVEw/viewform?embedded=true"
                                sx={{
                                    height: '55vh'
                                }} />
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleClickClose}>Close</Button>
                    </DialogActions>
                </Dialog>
        </AppBar>
        </ThemeProvider>
    );
}
export default ResponsiveAppBar;
