import React, {useState} from 'react';
import reactDOM from 'react-dom';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import {useTheme } from '@mui/material/styles';
import Button from '@mui/material/Button';
import { Typography } from '@mui/material';
import Iframe from 'react-iframe';
import CardMedia from '@mui/material/CardMedia';
import Box from '@mui/material/Box';

export default function PortfolioDialog({title, iframe, url, description, software}){
    const [open, setOpen] = useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    }
    const handleClickClose = () => {
        setOpen(false);
    }
    return(
        <div>
            <Box textAlign="center" sx={{paddingBottom: '5px'}}>
            <Button variant="outlined" onClick={handleClickOpen}>
                More
            </Button>
            </Box>
            <Dialog
                open={open}
                onClose={handleClickClose}
                fullWidth='l'
                maxWidth='xxl'
            >
                <DialogTitle variant="h2" align="center">
                    {title}
                </DialogTitle>
                <DialogContent sx={{height: '75vh'}}>
                    <DialogContent>
                        <div align="center" sx={{width: '100%'}}>
                            {/* Iframe must be styled */}
                            {{iframe} ? (
                            <CardMedia
                                component="iframe" 
                                src={url}
                                sx={{
                                    height: '55vh',
                                    width: '100%',
                                    overflow: 'hidden'
                                }}
                            />
                            ):(
                                <CardMedia
                                    component="img"
                                    src={url}
                                    align="center"
                                    sx={{height: '65vh', width: '100%', overflow: 'hidden'}}
                                />
                            )}
                            <Typography variant="h5">Description</Typography>
                            <Typography variant="p">{description}</Typography>
                            <Typography variant="h5">Technologies Used</Typography>
                            <Typography variant="p">{software}</Typography>
                        </div>
                    </DialogContent>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClickClose}>
                        Close
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );

}
